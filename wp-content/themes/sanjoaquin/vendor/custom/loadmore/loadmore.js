// esto se tomó de acá https://gist.github.com/aslamdoctor/e548b4721984676ff5019d208849d47a

jQuery(function ($) {
    $('.feed__loadmore').click(function () {

        var button = $(this),
            data = {
                'action': 'loadmore',
                'query': misha_loadmore_params.posts, // that's how we get params from wp_localize_script() function
                'page': misha_loadmore_params.current_page
            };

        $.ajax({
            url: misha_loadmore_params.ajaxurl, // AJAX handler
            data: data,
            type: 'POST',
            beforeSend: function (xhr) {
                button.html('Cargando <img class="feed__loadmore-icon" src="' + misha_loadmore_params.img_path +'">'); // change the button text, you can also add a preloader image
            },
            success: function (data) {
                if (data) {
                    // button.text('Más posts').before(data); // insert new posts
                    button.text('Ver más'); // insert new posts
                    data
                    $(data).insertBefore('.feed__loadmore').animate({
                        opacity: 1,
                    }, 250, function(){

                    });
                    misha_loadmore_params.current_page++;

                    if (misha_loadmore_params.current_page == misha_loadmore_params.max_page)
                        // button.removeClass('ui-btn_outline').text('No hay más publicaciones').addClass('ui-help-text'); // if last page, remove the button
                        $('<span class="feed__die ui-label-text">No hay más publicaciones</span>').insertBefore('.feed__loadmore').animate({
                            opacity: 1,
                            top: 0
                        }, 1200);
                        button.remove(); // if no data, remove the button as well


                    // you can also fire the "post-load" event here if you use a plugin that requires it
                    // $( document.body ).trigger( 'post-load' );
                } else {
                    button.remove(); // if no data, remove the button as well
                }
            }
        });
    });
});
