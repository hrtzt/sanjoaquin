<?php
/*
*
*  Contiene las funciones implementadas en el template.
*
* @package WPKit
* @author ALUMIN
* @version WPKIT 2.0
*/

/*******************************************************************************
WPKit */

	include_once( get_stylesheet_directory() . '/wpkit/config.php' );


/***************************************************************************
* Página de opciones ACF */

	if( function_exists('acf_add_options_page') ) {

		acf_add_options_page(array(
			'page_title' 	=> 'Info',
			'menu_title'	=> get_bloginfo('name') . ' info',
			'menu_slug' 	=> 'opciones-de-panel',
			'capability'	=> 'edit_posts',
			'redirect'		=> false,
			'icon_url'		=> 'dashicons-id',
		 'position'		=> '4',
		));

		// acf_add_options_sub_page(array(
		// 	'page_title' 	=> 'Herramientas',
		// 	'menu_title'	=> 'Herramientas',
		// 	'parent_slug'	=> 'opciones-de-panel',
		// ));

		// acf_add_options_sub_page(array(
		// 	'page_title' 	=> 'Opciones',
		// 	'menu_title'	=> 'Opciones',
		// 	'parent_slug'	=> 'options-wpkit',
		// ));

	}

/* ***********************************************************************************************************************
* Custom post type template *

	// 'Custom_post_type_name'

		if ( ! function_exists('wk_custom_post_type') ) {

			// Register Custom Post Type
			function wk_custom_post_type() {

				$ctp_name = 'articulos';
				$name = 'Articulos';
				$names = 'Artículos';

				$labels = array(
					'name'                  => _x( $names, 'Post Type General Name', 'wpkit_text_domain' ),
					'singular_name'         => _x( $name, 'Post Type Singular Name', 'wpkit_text_domain' ),
					'menu_name'             => __( $names, 'wpkit_text_domain' ),
					'name_admin_bar'        => __( $names, 'wpkit_text_domain' ),
					'archives'              => __( $names, 'wpkit_text_domain' ),
					'attributes'            => __( $names . ' Attributes', 'text_domain' ),
					'parent_item_colon'     => __( 'Superior', 'wpkit_text_domain' ),
					'all_items'             => __( 'Todo', 'wpkit_text_domain' ),
					'add_new_item'          => __( 'Añadir', 'wpkit_text_domain' ),
					'add_new'               => __( 'Nuevo', 'wpkit_text_domain' ),
					'new_item'              => __( 'Nuevo', 'wpkit_text_domain' ),
					'edit_item'             => __( 'Editar', 'wpkit_text_domain' ),
					'update_item'           => __( 'Actualizar', 'wpkit_text_domain' ),
					'view_item'             => __( 'Ver', 'wpkit_text_domain' ),
					'search_items'          => __( 'Buscar', 'wpkit_text_domain' ),
					'not_found'             => __( 'No se encontró', 'wpkit_text_domain' ),
					'not_found_in_trash'    => __( 'No se encontró en la papelera', 'wpkit_text_domain' ),
					'featured_image'        => __( 'Imagen destacada', 'wpkit_text_domain' ),
					'set_featured_image'    => __( 'Seleccionar como imagen destacada', 'wpkit_text_domain' ),
					'remove_featured_image' => __( 'Quitar imagen destacada', 'wpkit_text_domain' ),
					'use_featured_image'    => __( 'Usar como imagen destacada', 'wpkit_text_domain' ),
					'insert_into_item'      => __( 'Insertar', 'wpkit_text_domain' ),
					'uploaded_to_this_item' => __( 'Adjuntas a esta publicación', 'wpkit_text_domain' ),
					'items_list'            => __( 'Listado de elementos', 'wpkit_text_domain' ),
					'items_list_navigation' => __( 'Navegación por items', 'wpkit_text_domain' ),
					'filter_items_list'     => __( 'Filtrar por item en el listado', 'wpkit_text_domain' ),
				);
				$rewrite = array(
					'slug'                  => $ctp_name,
					'with_front'            => true,
					'pages'                 => true,
					'feeds'                 => true,
				);
				// Requiere habilitar capabilities para el rol de usuario
				// $capabilities = array(
				// 	'edit_post'          => 'edit_articulo',
				// 	'read_post'          => 'read_articulo',
				// 	'delete_post'        => 'delete_articulo',
				// 	'edit_posts'         => 'edit_articulos',
				// 	'edit_others_posts'  => 'edit_others_articulos',
				// 	'publish_posts'      => 'publish_articulos',
				// 	'read_private_posts' => 'read_private_articulos',
				// 	'create_posts'       => 'edit_articulos',
				// );
				$args = array(
					'label'                 => __( $name, 'wpkit_text_domain' ),
					'description'           => __( 'Publicaciones en el sitio', 'wpkit_text_domain' ),
					'labels'                => $labels,
					'supports'              => array(
													'title',
													'editor',
													'excerpt',
													'author',
													'thumbnail',
													'comments',
													'trackbacks',
													'revisions',
													'custom-fields',
													'page-attributes',
													'post-formats',
												),
					'taxonomies'            => array(
													'post_tag',
													'category'
												),
					'hierarchical'          => true,
					'public'                => true,
					'show_ui'               => true,
					'menu_position'         => 5,
					'menu_icon'             => 'dashicons-admin-post',
					'show_in_menu'          => true,
					'show_in_admin_bar'     => true,
					'show_in_nav_menus'     => true,
					'can_export'            => true,
					'has_archive'           => true,
					'exclude_from_search'   => false,
					'publicly_queryable'    => true,
					'rewrite'               => $rewrite,
					// 'capabilities'          => $capabilities,
					'show_in_rest'			=> true,
				);
				register_post_type( $ctp_name, $args );

			}
			add_action( 'init', 'wk_custom_post_type', 0 );
		}

		// Añade capabilities al rol de usuario
		// Necesario si se añade capabilities como argumento en el post type
		// function add_theme_caps() {
		//     // gets the administrator role
		//     $admins = get_role( 'editor' );

		//     $admins->add_cap( 'edit_articulo' );
		//     $admins->add_cap( 'read_articulo' );
		//     $admins->add_cap( 'delete_articulo' );
		//     $admins->add_cap( 'edit_articulos' );
		//     $admins->add_cap( 'edit_others_articulos' );
		//     $admins->add_cap( 'publish_articulos' );
		//     $admins->add_cap( 'read_private_articulos' );
		//     $admins->add_cap( 'edit_articulos' );

		// }
		// add_action( 'admin_init', 'add_theme_caps');


/*******************************************************************************
Tus funciones */

function img( $filename ) {

	echo get_template_directory_uri() . '/assets/img/' . $filename;

}

/*********************************************************************************
 * Adicionales
 * 
 */

function hojas_adicionales() {

	wp_register_style('adicional', get_template_directory_uri() . '/assets/css/adicional.css', '', '1.0');
	wp_enqueue_style('adicional');
	wp_register_script('adicional', get_template_directory_uri() . '/assets/js/adicional.js', array('jquery'), '1.0', true );
	wp_enqueue_script('adicional');

}

add_action('wp_enqueue_scripts', 'hojas_adicionales');

/*********************************************************************************
 * Lazy load posts
 */

function lazy_Load_posts() {
 
	global $wp_query; 
 
	// In most cases it is already included on the page and this line can be removed
 
	// register our main script but do not enqueue it yet
	wp_register_script( 'lazy-load', get_stylesheet_directory_uri() . '/vendor/custom/loadmore/loadmore.js', array('jquery') );
 
	// now the most interesting part
	// we have to pass parameters to myloadmore.js script but we can get the parameters values only in PHP
	// you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()
	wp_localize_script( 'lazy-load', 'misha_loadmore_params', array(
		'ajaxurl' 		=> site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
		'posts' 		=> json_encode( $wp_query->query_vars ), // everything about your loop is here
		'current_page' 	=> get_query_var( 'paged' ) ? get_query_var('paged') : 1,
		'max_page' 		=> $wp_query->max_num_pages,
		'img_path'		=> get_template_directory_uri() . '/assets/img/loader.gif'
	) );
 
 	wp_enqueue_script( 'lazy-load' );
}
 
add_action( 'wp_enqueue_scripts', 'lazy_Load_posts' );


function misha_loadmore_ajax_handler(){
 
	// prepare our arguments for the query
	$args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
	$args['post_status'] = 'publish';
 
	// it is always better to use WP_Query but not here
	query_posts( $args );
 
	if( have_posts() ) :
 
		// run the loop
		while( have_posts() ): the_post();

			?>

				<article <?php echo post_class( 'excerpt' ); ?> style="opacity: 0;">
					<div class="wk-cols">
						<div class="wk-col">

							<div class="excerpt__content">
				
								<h6 class="excerpt__date ui-paragraph_italic ui-text-pink"><?= get_the_date('j F Y'); ?></h6>
								<h1 class="excerpt__title ui-text-uppercase ui-title"> <?php the_title(); ?></h1>
								<p class="excerpt__content ui-paragraph_italic ui-paragraph_small"> <?= get_the_excerpt(); ?> </p>
								<p><a href="<?php the_permalink(); ?>" class="ui-btn excerpt__btn">Leer más</a></p>

							</div>
						
						</div>
						<div class="wk-col">

							<div class="excerpt__thumbnail">
								
								<?= the_post_thumbnail( 'thumbnail' ); ?>

							</div>
			
						</div>
					</div>
				</article>
			<?php 

		endwhile;

	endif;

	die; // here we exit the script and even no wp_reset_query() required!
}
 
 
 
add_action('wp_ajax_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}

