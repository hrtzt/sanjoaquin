// Librerías
//@prepros-prepend ../../vendor/fancybox/fancybox.js
//@prepros-prepend ../../vendor/slick/slick.js
//@prepros-prepend_ ../../vendor/tweenmax/TweenMax.min.js
//@prepros-prepend_ ../../vendor/scrollto/ScrollToPlugin.min.js
//@prepros-prepend ../../wpkit/js/site.js

// Components
//@prepros-prepend ../../components/footer/footer.js
//@prepros-prepend ../../components/header/header.js
//@prepros-prepend ../../components/slider/slider.js
//@prepros-prepend ../../components/offcanvas/offcanvas.js
//@prepros-prepend ../../components/builder/builder.js

// Modules
//@prepros-prepend ../../modules/style-guide/style-guide.js
//@prepros-prepend ../../modules/home/home.js
//@prepros-prepend ../../modules/blog/blog.js
//@prepros-prepend ../../modules/single/single.js
//@prepros-prepend ../../modules/conoce-irapuato/conoce-irapuato.js


