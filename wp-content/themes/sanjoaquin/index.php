<?php if( get_option( 'option_private_site' ) ) { if( ! is_user_logged_in() ) { get_template_part( 'wpkit/inc/login' ); return true; } }

/**
*
* Main page
*
* @package WPKit
* @author ALUMIN
* @copyright Copyright (C) Alumin.Agency
* @version WPKIT 3.0
*
*/

get_header(); 

	edit_post_link( '<span class="edit-post-link-text fas fa-edit"></span> Editar esto' );

	if( is_page('inicio') ) {
		get_template_part( 'modules/home/home' );
	} elseif( is_page('blog') || is_home() ) {
		get_template_part( 'modules/blog/blog' );
	} elseif( is_page('single') ) {
		get_template_part( 'modules/single/single' );
	} elseif( is_page('conoce-irapuato') ) {
		get_template_part( 'modules/conoce-irapuato/conoce-irapuato' );
 	} elseif( is_page() ) {
		get_template_part( 'modules/page/page' );
	} elseif( is_single() ) {
		get_template_part( 'modules/single/single' );
	} else {
		get_template_part('wpkit/systems/layouts/post');
	}

get_footer(); ?>
