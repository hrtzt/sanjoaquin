var booking_field = document.querySelectorAll('.booking input:not([type="submit"]):not([type="hidden"])');

for(var $i = 0; $i < booking_field.length; $i++) {

    booking_field[$i].addEventListener( "focus", function(event) {    

        this.parentElement.classList.add('is-active');

    });

}


/*
 * Slider galería
 */

$('.section-galeria__tabs').slick({
    asNavFor: '.section-galeria__nav', 
    rows: false,
    arrows: false,
    fade: true,
    adaptiveHeight: true
    // autoplay: 2500
});

$('.section-galeria__nav').slick({
    rows: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.section-galeria__tabs', 
    centerMode: true,
    focusOnSelect: true
});