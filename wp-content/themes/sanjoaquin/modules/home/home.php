<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

    <?php get_template_part( 'components/slider/slider' ); ?>

    <section id="section__agenda-tu-cita" class="wk-section ">
        <div class="wk-section-wrap">
    
            <div class="booking">
                <?= do_shortcode( '[contact-form-7 id="31" title="Agenda tu cita"] ' ) ;?>
            </div>
    
        </div>
    </section>

    <?php 
    
    /*
    * Builder de contenidos
    * 
    */

    get_template_part( 'components/builder/builder' ); ?>

<?php endwhile; else : echo "Aún no se ha publicado nada"; endif; ?>