
<?php 

$blog_id = false;

if( is_home() ) {
    $blog_id = 7;
}


?>

<section id="section-blog-header" class="wk-section">

    <div class="wk-section-wrap">

        <div class="heading">
    
            <?php if( get_field('blog_subtitulo', $blog_id) ) :?>
                <h3 class="heading__subtitle ui-title_italic"><?php the_field('blog_subtitulo', $blog_id); ?></h3>
            <?php endif; ?>
    
            <h1 class="heading__title ui-title-mega-big">BLOG</h1>
    
        </div>

    </div>


    <div class="slider-blog">
    
        <?php if( have_rows( 'slider', $blog_id ) ) : while( have_rows( 'slider', $blog_id ) ) : the_row(); ?>

            <div class="slide">

                    
                <img class="slide__image" src="<?php the_sub_field( 'slide_image', $blog_id ); ?>" alt="Banner">
                <div class="slide__content">
                    <?php the_sub_field( 'slide_content', $blog_id ); ?>
                </div>
                <?php //get_sub_field( 'slider_add_text' ); ?>
                <?php //the_sub_field( 'slide_content_pos' ); ?>

            
            </div>
        
        <?php endwhile; endif; ?>
    
    </div>

</section>

<section id="section-feed" class="wk-section feed">
    <div class="wk-section-wrap">

            <?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

                    <article <?php echo post_class( 'excerpt' ); ?>>
                        <div class="wk-cols">
                            <div class="wk-col">

                                <div class="excerpt__content">
                    
                                    <h6 class="excerpt__date ui-paragraph_italic ui-text-pink"><?= get_the_date('j F Y'); ?></h6>
                                    <h1 class="excerpt__title ui-text-uppercase ui-title"> <?php the_title(); ?></h1>
                                    <p class="excerpt__content ui-paragraph_italic ui-paragraph_small"> <?= get_the_excerpt(); ?> </p>
                                    <p><a href="<?php the_permalink(); ?>" class="ui-btn excerpt__btn">Leer más</a></p>

                                </div>
                            
                            </div>
                            <div class="wk-col">

                                <div class="excerpt__thumbnail">
                                    
                                    <?= the_post_thumbnail( 'thumbnail' ); ?>

                                </div>
                
                            </div>
                        </div>
                    </article>
            
            <?php endwhile; else : echo "Aún no se ha publicado nada"; endif; ?>

         <?php

            global $wp_query;
            
            $count_posts = wp_count_posts();

            // Checa si las entradas que se muestran es igual al número de posts publicados
            // Si es igual no muestra el botón Ver más
            if (  count($wp_query->posts) < $count_posts->publish ) {
                echo '<div class="ui-btn_outline feed__loadmore">Ver más</div>';    
            }

        ?>

    </div>
</section>

<section id="section-blog-footer" class="wk-section">

    <img src="<?= get_field('blog_foot', $blog_id ) ?>" alt="Imb">

</section>
