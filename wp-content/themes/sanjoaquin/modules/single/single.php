<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

    <img class="post__image" src="<?php the_field( 'single_header_img' ); ?>" alt="<?php bloginfo( 'name' ); ?>">

    <section id="section-post-content" class="wk-section">
        <div class="wk-section-wrap">

            <h1 class="post__title ui-title-big ui-text-uppercase"><?php the_title(); ?></h1>
            <p class="post_date ui-text-pink ui-label-text ui-paragraph_italic"><?php the_date( 'j M Y' ); ?></p>
            <div class="post__content">
                <?php the_content(); ?>
            </div>

        </div>
    </section>

    <section id="section-post-nav" class="wk-section">
        <div class="wk-section-wrap">

            <div class="post__nav">

                <div class="post__nav-link post__nav-prev-link">
                    <?php previous_post_link('<span class="post__nav-icon icon-arrow icon-arrow_lilac icon-arrow_prev"></span> %link', 'Anterior'); ?> 
                </div>

                <div class="post__nav-link post__nav-next-link">
                    <?php next_post_link('%link <span class="post__nav-icon icon-arrow icon-arrow_lilac icon-arrow_next"></span>', 'Siguiente'); ?> 
                </div>

            </div>

        </div>
    </section>

<?php endwhile; else : echo "Aún no se ha publicado nada"; endif; ?>