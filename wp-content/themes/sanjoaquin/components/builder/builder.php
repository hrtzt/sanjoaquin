<?php if( have_rows( 'bloque' ) ) : while( have_rows( 'bloque' ) ) : the_row(); ?>

    <?php if( get_row_layout() == 'heading' )  : ?>
        <section <?= $id = get_sub_field('id') ? 'id="section__' . get_sub_field('id') . '"' : '' ?> class="wk-section section-<?= get_row_layout(); ?>">
            <div class="wk-section-wrap">

                <div class="wk-cols">
                    <div class="wk-col">

                        <div class="heading">

                            <?php if( get_sub_field('encabezado_subtitulo') ) :?>
                                <h3 class="heading__subtitle ui-title_italic"><?php the_sub_field('encabezado_subtitulo'); ?></h3>
                            <?php endif; ?>

                            <?php if( get_sub_field('encabezado_titulo') ) :?>
                                <h1 class="heading__title ui-title-mega-big"><?php the_sub_field('encabezado_titulo'); ?></h1>
                            <?php endif; ?>

                            <?php if( get_sub_field('encabezado_contenido') ) :?>
                                <p class="heading__content ui-paragraph_italic"><?php the_sub_field('encabezado_contenido'); ?></p>
                            <?php endif; ?>

                        </div>

                    </div>

                        <?php if( get_sub_field('encabezado_link_file') || get_sub_field('encabezado_link_a') ) : ?>

                            <div class="wk-col-1">
                            
                                <a class="heading__link" href="<?= $link = get_sub_field('encabezado_link_type')  == 'Archivo' ? get_sub_field('encabezado_link_file') : get_sub_field('encabezado_link_a'); ?>" target="_blank">
                                    <?= $link_img = get_sub_field('encabezado_link_icon') ? '<img class=""section-heading__link-icon src="' . get_template_directory_uri() . '/assets/img/icon-download.png" alt="Icono">' : ''; ?>
                                    <span class="ui-paragraph_italic"><?php the_sub_field('encabezado_link_titulo') ?></span>
                                </a>

                            </div>

                        <?php endif; ?>
                </div>

            </div>
        </section>
    <?php elseif( get_row_layout() == 'galeria' )  : ?>

        <section <?= $id = get_sub_field('id') ? 'id="section__' . get_sub_field('id') . '"' : '' ?> class="wk-section section-<?= get_row_layout(); ?>">

            <div class="wk-section-wrap">

                <div class="wk-cols">
                    <div class="wk-col-3">
                    
                        <div class="heading">
    
                            <?php if( get_sub_field('galería_subtitulo') ) :?>
                                <h3 class="heading__subtitle ui-title_italic"><?php the_sub_field('galería_subtitulo'); ?> </h3>
                            <?php endif; ?>
    
                            <?php if( get_sub_field('galería_título') ) :?>
                                <h3 class="heading__title ui-title-mega-big"> <?php the_sub_field('galería_título'); ?> </h3>
                            <?php endif; ?>

                        </div>

                    </div>
                    <div class="wk-col-4">

                        <div class="section-galeria__nav">
                            <?php $galeria = get_sub_field('galeria_imagenes'); foreach( $galeria as $image ) : ?>

                                <span class="section-galeria__nav-item"><?= $image['title'] ?></span>
                                    
                            <?php endforeach; ?>  
                        </div>

                    </div>
                </div>
                
            </div>

            <?php if( get_sub_field('galeria_imagenes') ) :?>

                <div class="section-galeria__tabs">
                    <?php $galeria = get_sub_field('galeria_imagenes'); foreach( $galeria as $image ) : ?>

                        <div class="slider-gal-item">
                            <img class="wk-img-responsive" src="<?= $image['sizes']['large'] ?>" alt="<?= $image['title'] ?>">
                        </div>

                    <?php endforeach; ?>         
                </div>

            <?php endif; ?>

        </section>

    <?php elseif( get_row_layout() == 'propiedades' )  : ?>

        <section <?= $id = get_sub_field('id') ? 'id="section__' . get_sub_field('id') . '"' : '' ?> class="wk-section section-<?= get_row_layout(); ?>">
            <div class="wk-section-wrap">

                <div class="propiedades">

                    <?php 

                        function propiedades( $field, $field_gal ) {

                            ?>

                                <div class="propiedades__block propiedades__<?= $propiedad_type = $field == 'propiedades_disponibles' ? 'disponibles' : 'vendidas' ?>">
                        
                                    <div class="wk-cols">
                                        <div class="wk-col-1">
    
                                            <div class="propiedades__data">
                                                
                                                <h3 class="propiedades__data-num"><?php the_sub_field( $field ) ?></h3>
                                                <h2 class="propiedades__data-label">Disponibles</h2>
                                                
                                            </div>
            
                                        </div>
                                        <div class="wk-col">
            
                                            <div class="propiedades__gal">
                                                
                                                <?php foreach( get_sub_field( $field_gal ) as $img ) : ?>
    
                                                    <img class="propiedades__gal-img" src="<?= $img['sizes']['thumbnail'] ?>" alt="<?= $img['title'] ?>">
    
                                                <?php endforeach; ?>
    
                                            </div>
                                            
                                        </div>
                                    </div>

                                </div>


                            <?php

                        }

                        propiedades( 'propiedades_disponibles', 'propiedades_disponibles_gal' );
                        propiedades( 'propiedades_vendidas', 'propiedades_vendidas_gal' );

                    ?>

                </div>

            </div>
        </section>

    <?php elseif( get_row_layout() == 'mapa' )  : ?>

        <section <?= $id = get_sub_field('id') ? 'id="section__' . get_sub_field('id') . '"' : '' ?> class="wk-section section-<?= get_row_layout(); ?>">
            <div class="mapa">
                <img src="<?= get_sub_field( 'mapa_thumbnail' ); ?>" alt="" class="mapa__thumbnail">
                <?= get_sub_field('mapa_embed') ?>
            </div>
        </section>

    <?php elseif( get_row_layout() == 'imagen' )  : ?>

        <section <?= $id = get_sub_field('id') ? 'id="section__' . get_sub_field('id') . '"' : '' ?> class="wk-section section-<?= get_row_layout(); ?>">
            <img class="wk-img-responsive" src="<?php the_sub_field('imagen') ?>" alt="">
        </section>

    <?php elseif( get_row_layout() == 'slider_alt' )  : ?>

        <section <?= $id = get_sub_field('id') ? 'id="section__' . get_sub_field('id') . '"' : '' ?> class="wk-section section-<?= get_row_layout(); ?>">
            <div class="wk-section-wrap">

                <div id="slider-alt__nav"></div>
                
                <div class="slider-alt">

                    <?php 
                    
                        $slider_alt_imgs = get_sub_field('slider_alt_gal'); 
                        
                        for( $x = 0; $x < count($slider_alt_imgs); $x++ ) {

                            if( $x % 2 == 0 ) {

                                ?>
                                
                                    <div class="slider-alt__item">
                                        <div class="slider-alt__item-container">
                                            <img class="slider-alt__img slider-alt__img_top" src="<?= $slider_alt_imgs[$x]['sizes']['thumbnail']; ?>" alt="<?= $slider_alt_imgs[$x]['title']; ?>">
                                            <img class="slider-alt__img slider-alt__img_bot" src="<?= $slider_alt_imgs[$x+1]['sizes']['large']; ?>" alt="<?= $slider_alt_imgs[$x]['title']; ?>">
                                        </div>
                                    </div>

                                <?php

                            }

                        } 
                    
                    ?>

                </div>
                
            </div>
        </section>

    <?php elseif( get_row_layout() == 'timeline' )  : ?>

        <section <?= $id = get_sub_field('id') ? 'id="section__' . get_sub_field('id') . '"' : '' ?> class="wk-section section-<?= get_row_layout(); ?>">
            <div class="wk-section-wrap">

                <?php if ( get_sub_field('timeline_bg') ) : ?>
                    <img class="timeline__bg" src="<?php the_sub_field('timeline_bg'); ?>">
                <?php endif; ?>

                
                <div class="slider-timeline timeline">

                    <?php if( have_rows( 'timeline_fechas' ) ) : while( have_rows( 'timeline_fechas' ) ) : the_row(); ?>
                    
                        <div class="timeline__slide">

                            <div class="timeline__item">
                                <h5 class="timeline__fecha ui-title-big"> <?= get_sub_field( 'timeline_fecha' ) ?> </h5>
                                <h3 class="timeline__title ui-title ui-paragraph_italic"><?= get_sub_field( 'timeline_subtitle' ) ?></h3>
                                <p class="timeline__content ui-paragraph_italic"><?= get_sub_field( 'timeline_content' ) ?></p>
                            </div>
                        </div>
                    
                    <?php endwhile; endif; ?>

                </div>
                
                <div class="timeline__dots"></div>

            </div>
        </section>

    
    <?php endif; ?>

<?php endwhile; endif; ?>