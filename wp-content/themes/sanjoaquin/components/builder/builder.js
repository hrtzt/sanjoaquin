$('.slider-alt').slick({
    rows: false,
    dots: true,
    prevArrow: '',
    appendArrows: $('#slider-alt__nav'),
    appendDots: $('#slider-alt__nav'),
});


/*
 * Slider timeline
 *
 */

    
 $('.slider-timeline').slick({
    vertical: true,
    rows: false,
    infinite: false,
    nextArrow: '<span class="slider__arrow-next timeline__arrow slider__arrow"></span>',
    prevArrow: '<span class="slider__arrow-prev timeline__arrow slider__arrow"></span>',
    dots: true, 
    appendDots: $('.timeline__dots'), 
    verticalSwiping: true
});


$('.mapa__thumbnail').click(function(){

    $(this).animate({
        opacity: 0,
    }, 600, function(){
        $(this).hide();
    });

});

    