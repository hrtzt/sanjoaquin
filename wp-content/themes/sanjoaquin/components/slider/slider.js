$('.slider').slick({
    fade: true,
    rows: false,
    nextArrow: '<span class="slider__arrow-next slider__arrow"></span>',
    prevArrow: '<span class="slider__arrow-prev slider__arrow"></span>',
});

$('.slider-blog').slick({
    fade: true,
    rows: false,
    nextArrow: '<span class="slider__arrow-next slider__arrow"></span>',
    prevArrow: '<span class="slider__arrow-prev slider__arrow"></span>',
});