<section id="section__slider" class="wk-section ">

    <div class="slider">
    
        <?php if( have_rows( 'slider' ) ) : while( have_rows( 'slider' ) ) : the_row(); ?>

            <div class="slide">

                <div class="wk-section-wrap">
                    
                        <img class="slide__image" src="<?php the_sub_field( 'slide_image' ); ?>" alt="Banner">
                        <div class="slide__content">
                            <?php the_sub_field( 'slide_content' ); ?>
                        </div>
                        <?php //get_sub_field( 'slider_add_text' ); ?>
                        <?php //the_sub_field( 'slide_content_pos' ); ?>

                </div>
            
            </div>
        
        <?php endwhile; endif; ?>
    
    </div>

</section>