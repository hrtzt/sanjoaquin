<footer id="section__main-footer" class="wk-section">
    <div class="wk-section-wrap">

        <div class="main-footer">
            <div class="main-footer__logo">
                <?php if( get_option('wk_custom_logo_main') ) : ?>
                    <img class="main-footer__logo-img" src="<?php echo get_option('wk_custom_logo_main'); ?>" alt="<?= bloginfo( 'name' ); ?>">
                <?php else : ?>
                    <h1><?= bloginfo( 'name' ); ?></h1>
                <?php endif; ?>
            </div>
            <address class="address">
                <span class="address__block address__tel">
                    <span class="address__label">Tel: </span>
                    <span><a href="tel:<?= get_field( 'info_telefono', 'option' ); ?>"><?= get_field( 'info_telefono', 'option' ); ?></a></span>
                </span>
                <span class="address__div">//</span>
                <span class="address__block address__dir">
                    <div class="address__label">Dirección: <span><?= get_field( 'Info_direccion', 'option' ); ?></span></div>
                </span>
            </address>
            <div class="s-links">
                <?php if(get_field( 'info_facebook', 'option' )) : ?>
                    <a href="<?= get_field( 'info_facebook', 'option' ); ?>" target="_blank" class="s-link">
                        <i class="s-link__icon fab fa-facebook-f"></i>
                        <span class="s-link__content"><?= get_field( 'info_facebook_copy', 'option' ); ?></span>
                    </a>
                <?php endif; ?>
                <?php if(get_field( 'info_instagram', 'option' )) : ?>
                    <a href="<?= get_field( 'info_instagram', 'option' ); ?>" target="_blank" class="s-link">
                        <i class="s-link__icon fab fa-instagram"></i>
                    <span class="s-link__content"><?= get_field( 'info_instagram_copy', 'option' ); ?></span>
                <?php endif; ?>
                </a>
            </div>
        </div>

        
    </div>
</footer>

<section id="section__legals" class="wk-section">
    <div class="wk-section-wrap">
        
        <div class="legals">
            <div class="wk-cols">
            
                <div class="wk-col">
        
                    <p class="legals__nav-copy">Copyright &copy; 2019 Todos los derechos reservados</p>
            
                </div>
            
                <div class="wk-col">
        
                    <nav class="legals__nav">
                        <ul>
                            <li class="legals__nav-item"><a href="<?= get_bloginfo( 'url' ); ?>/aviso-de-privacidad">AVISO DE PRIVACIDAD</a></li>
                            <li class="legals__nav-item"><a href="<?= get_bloginfo( 'url' ); ?>/terminos-y-condiciones">TÉRMINOS Y CONDICIONES</a></li>
                        </ul>
                    </nav>
            
                </div>
            
            </div>
        </div>

    </div>
</section>