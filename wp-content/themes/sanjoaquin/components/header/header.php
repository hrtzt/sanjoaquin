<section id="section-main-header" class="wk-section">
        
    <header class="main-header">
        <div class="main-heder__cols wk-cols-me">
            <div class="main-heder__col main-header__col_left wk-col">
            
                <div class="main-header__logo">
                    <?php if( get_option('wk_custom_logo_main') ) : ?>
                        <a href="<?= get_bloginfo( 'url' ); ?>">
                            <img class="main-header__logo-img" src="<?= $logo = is_page('conoce-irapuato') ? get_option('wk_custom_logo_complementary') : is_home() ? get_option('wk_custom_logo_alt') : get_option('wk_custom_logo_main') ?>" alt="<?= bloginfo( 'name' ); ?>">
                        </a>
                    <?php else : ?>
                        <h1><?= bloginfo( 'name' ); ?></h1>
                    <?php endif; ?>
                </div>
            
            </div>
            <div class="main-header__col main-header__col_right wk-col">

                <?php 
                
                $nav_args = array(
                    'theme_location'  => 'main-nav',
                    'container'       => 'nav',
                    'container_class' => 'main-nav',
                    'menu_class'      => 'main-nav__menu',
                    'menu_id'         => '',
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'depth'           => 0, // Cuantos niveles de jerarquía se incluirán 0 es todos. -1 imprime todos los niveles en uno mismo.
                    'walker'          => ''
                );
                
                wp_nav_menu( $nav_args ); ?>
                                    
            </div>
            <div id="offcanvas-icon" class="main-header__col wk-col main-header__offcanvas wk-m">
                <span class="main-header__offcanvas-icon fas fa-bars"></span>
            </div>
        </div>
    </header>

</section>

