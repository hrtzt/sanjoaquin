// Off canvas Menu

$('.main-header__offcanvas-icon').click(function(){
    $('body').toggleClass('offcanvas-is-active');
});

$(window).on('resize', function () {
    var win = $(this);
    if (win.width() < 770) {
    } else {
        $('body').removeClass('offcanvas-is-active');
    }
});